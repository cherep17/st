$( document ).ready(function() {
    (function () {
        $('.js-main_nav').on('click', function(e) {
            e.preventDefault();
            const link = e.target.closest('.js-main_nav-link');
            if (link) {
                $('.js-main_nav').removeClass('main_nav--active');
                const sectionName = '#' + link.dataset.section;
                const position = sectionName === '#contacts' ? $(sectionName).offset().top : $(sectionName).offset().top - 100;
                $('html, body').stop().animate({ scrollTop: position }, 500);
            }
        });
    })();

    (function () {
        $('.js-main_nav-show').on('click', function () {
            $('.js-main_nav').addClass('main_nav--active');
        });

        $('.js-main_nav-list')
            .prepend('<div class="main_nav-close_wrapper"><button class="main_nav-close js-main_nav-close"></button></div>');

        $('.js-main_nav-close').on('click', function () {
            $('.js-main_nav').removeClass('main_nav--active');
        });

        $('.js-main_nav').on('click', function (e) {
            if ($(e.target).hasClass('js-main_nav')) {
                $(this).removeClass('main_nav--active');
            }
        });
    })();

    (function () {
        $('.owl-carousel').each(function () {
            $(this).owlCarousel({
                loop:true, //Зацикливаем слайдер
                nav:true, //Отключение навигации
                autoplay:true, //Автозапуск слайдера
                smartSpeed:1000, //Время движения слайда
                autoplayTimeout:5000, //Время смены слайда
                responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                    0:{
                        items:1
                    }
                }
            });
        });
    })();
    $(window).on('resize', function() {
        const height = Math.ceil($('.owl-stage-outer').outerHeight(false) / 2) - Math.ceil($('.owl-nav').outerHeight(false) / 2);
        $('.owl-nav').each(function () {
            $(this).css({'top': height + 'px'});
        });
    });
});

$(window).on('load', function() {
    const height = Math.ceil($('.owl-stage-outer').outerHeight(false) / 2) - Math.ceil($('.owl-nav').outerHeight(false) / 2);
    $('.owl-nav').each(function () {
        $(this).css({'top': height + 'px'});
    });
});




